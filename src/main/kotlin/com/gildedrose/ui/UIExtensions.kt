package com.gildedrose.ui

import com.gildedrose.data.Item


fun String.fixedChars(numberOfChars : Int) : String{

    var string = this

    if (string.length > numberOfChars){
        string = this.substring(0, if (length >= numberOfChars) numberOfChars else length)
    }
    while(string.length < numberOfChars){
        string = "$string "
    }

    return string
}


fun Array<Item>.prettyPrint(){
    //Print title
    println(" " + "-".repeat(64))
    println(" | ${"name".fixedChars(40)} | ${"sellIn".fixedChars(7)} | ${"quality".fixedChars(7)} | ")
    println(" " + "-".repeat(64))
    //Print elements
    for (item in this) {
        println(" | ${item.name.fixedChars(40)} | ${item.sellIn.toString().fixedChars(7)} | ${item.quality.toString().fixedChars(7)} | ")
    }
    println(" " + "-".repeat(64))

}