package com.gildedrose.data.enum

enum class RuledItemName(val value : String) {
    AGED_BRIE("Aged Brie"),
    ELIXIR_OF_THE_MONGOOSE("Elixir of the Mongoose"),
    SULFURAS("Sulfuras, Hand of Ragnaros"),
    BACKSTAGE_PASSES_TO_A_TAFKAL80ETC_CONCERT("Backstage passes to a TAFKAL80ETC concert"),
    CONJURED_MANA_CAKE("Conjured Mana Cake")
}