package com.gildedrose.business

import com.gildedrose.data.Item
import com.gildedrose.data.enum.RuledItemName

/**
======================================
Gilded Rose Requirements Specification
======================================

Hi and welcome to team Gilded Rose. As you know, we are a small inn with a prime location in a
prominent city ran by a friendly innkeeper named Allison. We also buy and sell only the finest goods.
Unfortunately, our goods are constantly degrading in quality as they approach their sell by date. We
have a system in place that updates our inventory for us. It was developed by a no-nonsense type named
Leeroy, who has moved on to new adventures. Your task is to add the new feature to our system so that
we can begin selling a new category of items. First an introduction to our system:

- All items have a SellIn value which denotes the number of days we have to sell the item
- All items have a Quality value which denotes how valuable the item is
- At the end of each day our system lowers both values for every item

Pretty simple, right? Well this is where it gets interesting:

- Once the sell by date has passed, Quality degrades twice as fast
- The Quality of an item is never negative
- "Aged Brie" actually increases in Quality the older it gets
- The Quality of an item is never more than 50
- "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
- "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
Quality drops to 0 after the concert

We have recently signed a supplier of conjured items. This requires an update to our system:

- "Conjured" items degrade in Quality twice as fast as normal items

Feel free to make any changes to the UpdateQuality method and add any new code as long as everything
still works correctly. However, do not alter the Item class or Items property as those belong to the
goblin in the corner who will insta-rage and one-shot you as he doesn't believe in shared code
ownership (you can make the UpdateQuality method and Items property static if you like, we'll cover
for you).

Just for clarification, an item can never have its Quality increase above 50, however "Sulfuras" is a
legendary item and as such its Quality is 80 and it never alters.
 **/
class GildedRose(var items: Array<Item>) {

    private val updateQualityValueDefault = -1              //Default quality update
    private val qualityValueMaxDefault = 50                 //Default max quality
    private val qualityValueMinDefault = 0                  //Default min quality
    private val updateQualityValueMultiplierDefault = 1     //Default quality multiplier
    private val updateQualityValueMultiplierExpired = 2     //On expired multiplier


    /**
     *
     * This method it will update the every [Item] inside the class Array of Items [items] n each iteration
     *
     * To don't affect the tests from the Goblin the name will remain as updateQuality but maybe updateItem will be a
     * better name since also is updating the sellIn value inside
     *
     * The DEFAULT rules for updating an item with every iteration:
     * Item sellIn it will be reduced by 1
     * Item quality will be reduced by 1
     *
     * Still there is different conditions according to the different products.
     * The segregation of the different rules will be done according the product name
     * All the items with specific rules are enumerated in the class [RuledItemName]
     *
     */
    fun updateQuality() {

        for (item in items) {

            when (item.name) {

                RuledItemName.AGED_BRIE.value -> {
                    item.sellIn--
                    val multiplier = getQualityUpdateMultiplier(item.isExpired())

                    // "Aged Brie" actually increases in Quality the older it gets (update value 1)
                    item.quality = updateItemQuality(
                        quality = item.quality,
                        updateValue = 1,
                        multiplier = multiplier
                    )
                }

                RuledItemName.BACKSTAGE_PASSES_TO_A_TAFKAL80ETC_CONCERT.value -> {
                    item.sellIn--

                    //"Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
                    // Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
                    // Quality drops to 0 after the concert
                    item.quality = when {
                        item.sellIn in 0..4 -> updateItemQuality(quality = item.quality, updateValue = 3)
                        item.sellIn in 5..9 -> updateItemQuality(quality = item.quality, updateValue = 2)
                        item.sellIn >= 10 -> updateItemQuality(quality = item.quality, updateValue = 1)
                        //Can be done just by setting it to 0 instead to use the method more efficient but using the method gives consistency to the logic
                        else -> updateItemQuality(quality = 0)
                    }
                }

                // "Conjured" items degrade in Quality twice as fast as normal items (*2)
                RuledItemName.CONJURED_MANA_CAKE.value -> {
                    item.sellIn--
                    val multiplier = getQualityUpdateMultiplier(item.isExpired()) * 2
                    item.quality =
                        updateItemQuality(
                            quality = item.quality,
                            multiplier = multiplier
                        )

                }
                // Sulfuras", being a legendary item, never has to be sold or decreases in Quality update value set to 0 (no sellIn decrease)
                // Sulfuras" is a legendary item and as such its Quality is 80, it never alters. (Quality remains 80)
                RuledItemName.SULFURAS.value -> {
                    item.quality = updateItemQuality(
                        quality = 80,       // This is a feature. Is setting the quality automatically to 80 since is a rule. But probably throw an exception could be a solution to prevent
                                            // the insertion of wrong data
                        updateValue = 0,
                        maxValue = 80
                    )
                }

                else -> {
                    item.sellIn--
                    val multiplier = getQualityUpdateMultiplier(item.isExpired())
                    item.quality = updateItemQuality(
                        item.quality,
                        multiplier = multiplier
                    )
                }

            }
        }

    }

    /**
     * (This method is public for testing purposes)
     *
     * Receiving a [quality], it will update the value according an [updateValue] and [multiplier] (witch multiplies the update value)
     * restricting by a max [maxValue] and a min [minValue]
     *
     * [updateValue] [maxValue] [minValue] will use default if is not passed by param
     * The minimum quality is fixed to the default const var [qualityValueMinDefault]
     *
     * @param quality initial item quality
     * @param updateValue value to add to the quality
     * @param multiplier value to multiply to update value
     * @param maxValue value max cap
     * @param minValue value min cap
     */
    fun updateItemQuality(
        quality: Int,
        updateValue: Int = updateQualityValueDefault,
        multiplier: Int = updateQualityValueMultiplierDefault,
        maxValue: Int = qualityValueMaxDefault,
        minValue: Int = qualityValueMinDefault
    ): Int {

        var newQuality = quality + (updateValue * multiplier)

        if (newQuality < minValue) newQuality = minValue
        else if (newQuality > maxValue) newQuality = maxValue

        return newQuality

    }

    //Once the sell by date has passed, Quality degrades twice as fast

    private fun getQualityUpdateMultiplier(applyDoubleMultiplier: Boolean) =
        if (applyDoubleMultiplier) updateQualityValueMultiplierExpired else updateQualityValueMultiplierDefault

    /**
     * Item is expired if sellIn is below 0
     */
    private fun Item.isExpired(): Boolean = sellIn < 0

    /**
     * TODO REMOVE THIS METHOD
     *
     * Right now this method is just used in TESTS to compare the speed performance between the old and new methods
     * On remove remove also the test
     */

    fun updateQualityOld() {

        for (i in items.indices) {
            if (items[i].name != "Aged Brie" && items[i].name != "Backstage passes to a TAFKAL80ETC concert") {
                if (items[i].quality > 0) {
                    if (items[i].name != RuledItemName.SULFURAS.value) {
                        items[i].quality = items[i].quality - 1
                    }
                }
            } else {
                if (items[i].quality < 50) {
                    items[i].quality = items[i].quality + 1

                    if (items[i].name == "Backstage passes to a TAFKAL80ETC concert") {
                        if (items[i].sellIn < 11) {
                            if (items[i].quality < 50) {
                                items[i].quality = items[i].quality + 1
                            }
                        }

                        if (items[i].sellIn < 6) {
                            if (items[i].quality < 50) {
                                items[i].quality = items[i].quality + 1
                            }
                        }
                    }
                }
            }

            if (items[i].name != "Sulfuras, Hand of Ragnaros") {
                items[i].sellIn = items[i].sellIn - 1
            }

            if (items[i].sellIn < 0) {
                if (items[i].name != "Aged Brie") {
                    if (items[i].name != "Backstage passes to a TAFKAL80ETC concert") {
                        if (items[i].quality > 0) {
                            if (items[i].name != "Sulfuras, Hand of Ragnaros") {
                                items[i].quality = items[i].quality - 1
                            }
                        }
                    } else {
                        items[i].quality = items[i].quality - items[i].quality
                    }
                } else {
                    if (items[i].quality < 50) {
                        items[i].quality = items[i].quality + 1
                    }
                }
            }
        }


    }




}

