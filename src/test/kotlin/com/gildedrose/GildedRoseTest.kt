package com.gildedrose

import com.gildedrose.business.GildedRose
import com.gildedrose.data.Item
import com.gildedrose.data.enum.RuledItemName
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.system.measureNanoTime

internal class GildedRoseTest {

    @Test
    fun `verify quality decreases as double when sellIn is negative`() {
        val items = arrayOf(
            Item(RuledItemName.ELIXIR_OF_THE_MONGOOSE.value, -1, 20)
        )
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(18, app.items.first().quality)
    }


    @Test
    fun `verify product AGED_BRIE quality update`() {
        val items = arrayOf(
            Item(RuledItemName.AGED_BRIE.value, 20, 20)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(21, app.items.first().quality)

        app.updateQuality()
        assertEquals(22, app.items.first().quality)
    }

    @Test
    fun `verify product AGED_BRIE quality update max`() {
        val items = arrayOf(
            Item(RuledItemName.AGED_BRIE.value, 20, 49)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(50, app.items.first().quality)

        app.updateQuality()
        assertEquals(50, app.items.first().quality)
    }

    @Test
    fun `verify product AGED_BRIE expired quality update`() {
        val items = arrayOf(
            Item(RuledItemName.AGED_BRIE.value, 1, 20)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(21, app.items.first().quality)

        app.updateQuality()
        assertEquals(23, app.items.first().quality)

        app.updateQuality()
        assertEquals(25, app.items.first().quality)
    }


    @Test
    fun `verify product SULFURAS quality doesn't change`() {
        val items = arrayOf(
            Item(RuledItemName.SULFURAS.value, 20, 80)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(80, app.items.first().quality)

        app.updateQuality()
        assertEquals(80, app.items.first().quality)
    }

    @Test
    fun `verify product BACKSTAGE_PASSES_TO_A_TAFKAL80ETC_CONCERT quality check 10 days increase quality`() {
        val items = arrayOf(
            Item(RuledItemName.BACKSTAGE_PASSES_TO_A_TAFKAL80ETC_CONCERT.value, 12, 20)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(21, app.items.first().quality)

        app.updateQuality()
        assertEquals(22, app.items.first().quality)

        app.updateQuality()
        assertEquals(24, app.items.first().quality)

        app.updateQuality()
        assertEquals(26, app.items.first().quality)
    }

    @Test
    fun `verify product BACKSTAGE_PASSES_TO_A_TAFKAL80ETC_CONCERT quality test last 5 days until expiration`() {
        val items = arrayOf(
            Item(RuledItemName.BACKSTAGE_PASSES_TO_A_TAFKAL80ETC_CONCERT.value, 6, 0)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(2, app.items.first().quality)

        app.updateQuality()
        assertEquals(5, app.items.first().quality)

        app.updateQuality()
        assertEquals(8, app.items.first().quality)

        app.updateQuality()
        assertEquals(11, app.items.first().quality)

        app.updateQuality()
        assertEquals(14, app.items.first().quality)

        app.updateQuality()
        assertEquals(17, app.items.first().quality)

        app.updateQuality()
        assertEquals(0, app.items.first().quality)

        app.updateQuality()
        assertEquals(0, app.items.first().quality)

        app.updateQuality()
        assertEquals(0, app.items.first().quality)
    }

    @Test
    fun `verify product CONJURED_MANA_CAKE quality update`() {
        val items = arrayOf(
            Item(RuledItemName.CONJURED_MANA_CAKE.value, 20, 20)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(18, app.items.first().quality)

        app.updateQuality()
        assertEquals(16, app.items.first().quality)
    }

    @Test
    fun `verify product CONJURED_MANA_CAKE quality update min`() {
        val items = arrayOf(
            Item(RuledItemName.CONJURED_MANA_CAKE.value, 20, 1)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(0, app.items.first().quality)

        app.updateQuality()
        assertEquals(0, app.items.first().quality)
    }

    @Test
    fun `verify product ELIXIR_OF_THE_MONGOOSE quality update`() {
        val items = arrayOf(
            Item(RuledItemName.ELIXIR_OF_THE_MONGOOSE.value, 20, 20)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(19, app.items.first().quality)

        app.updateQuality()
        assertEquals(18, app.items.first().quality)
    }

    @Test
    fun `verify product ELIXIR_OF_THE_MONGOOSE quality update min`() {
        val items = arrayOf(
            Item(RuledItemName.ELIXIR_OF_THE_MONGOOSE.value, 20, 1)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(0, app.items.first().quality)

        app.updateQuality()
        assertEquals(0, app.items.first().quality)
    }

    @Test
    fun `verify product CONJURED_MANA_CAKE decreases quality twice faster`() {
        val items = arrayOf(
            Item(RuledItemName.CONJURED_MANA_CAKE.value, 20, 40)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(38, app.items.first().quality)

        app.updateQuality()
        assertEquals(36, app.items.first().quality)
    }

    @Test
    fun `verify product CONJURED_MANA_CAKE expired decreases quality 4 times faster`() {
        val items = arrayOf(
            Item(RuledItemName.CONJURED_MANA_CAKE.value, -1, 40)
        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(36, app.items.first().quality)

        app.updateQuality()
        assertEquals(32, app.items.first().quality)
    }


    @Test
    fun `verify sellIn always deceases except for SULFURAS`() {
        val items = arrayOf(
            Item("New item", 1, 40),
            Item(RuledItemName.CONJURED_MANA_CAKE.value, 1, 40),
            Item(RuledItemName.BACKSTAGE_PASSES_TO_A_TAFKAL80ETC_CONCERT.value, 1, 40),
            Item(RuledItemName.AGED_BRIE.value, 1, 40),
            Item(RuledItemName.ELIXIR_OF_THE_MONGOOSE.value, 1, 40),
            Item(RuledItemName.SULFURAS.value, 1, 40)

        )
        val app = GildedRose(items)

        app.updateQuality()
        assertEquals(0, app.items[0].sellIn)
        assertEquals(0, app.items[1].sellIn)
        assertEquals(0, app.items[2].sellIn)
        assertEquals(0, app.items[3].sellIn)
        assertEquals(0, app.items[4].sellIn)
        assertEquals(1, app.items[5].sellIn)

        app.updateQuality()
        assertEquals(-1, app.items[0].sellIn)
        assertEquals(-1, app.items[1].sellIn)
        assertEquals(-1, app.items[2].sellIn)
        assertEquals(-1, app.items[3].sellIn)
        assertEquals(-1, app.items[4].sellIn)
        assertEquals(1, app.items[5].sellIn)

    }


    @Test
    fun `test efficiency between both methods`() {

        val list = mutableListOf<Item>()
        val dataBlock = arrayOf(
            Item("+5 Dexterity Vest", 10, 20), //
            Item("Aged Brie", 2, 0), //
            Item("Elixir of the Mongoose", 5, 7), //
            Item("Sulfuras, Hand of Ragnaros", 0, 80), //
            Item("Sulfuras, Hand of Ragnaros", -1, 80),
            Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
            Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
            Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
            Item("Backstage passes to a TAFKAL80ETC concert", 5, 10),
            Item("Conjured Mana Cake", 3, 6)
        )
        for (i in 0 .. 100000){
            list.addAll(dataBlock)
        }

        val mockData = list.toTypedArray()

        val glided1 = GildedRose(mockData.copyOf())
        val glided2 = GildedRose(mockData.copyOf())

        val oldMethodTime = measureNanoTime { glided1.updateQualityOld() }
        val newMethodTime = measureNanoTime { glided2.updateQuality() }

        println("oldMethodTime : $oldMethodTime")
        println("newMethodTime : $newMethodTime")
        println("diff : ${oldMethodTime - newMethodTime}")

        Assertions.assertTrue(
            oldMethodTime > newMethodTime
        )
    }

    @Test
    fun `test updateItemQuality default quality decreasing`() {

        val glideRose = GildedRose(emptyArray())
        assertEquals(4, glideRose.updateItemQuality(5))
        assertEquals(0, glideRose.updateItemQuality(1))
        assertEquals(0, glideRose.updateItemQuality(0))

    }

    @Test
    fun `test updateItemQuality default quality increasing`() {
        val glideRose = GildedRose(emptyArray())
        assertEquals(1, glideRose.updateItemQuality(0, 1))
        assertEquals(50, glideRose.updateItemQuality(49, 1))
        assertEquals(50, glideRose.updateItemQuality(50, 1))
    }


    @Test
    fun `test updateItemQuality default quality decreasing with min`() {

        val glideRose = GildedRose(emptyArray())
        assertEquals(0, glideRose.updateItemQuality(1))
        assertEquals(0, glideRose.updateItemQuality(0))

    }

    @Test
    fun `test updateItemQuality default quality increasing with max`() {

        val glideRose = GildedRose(emptyArray())
        assertEquals(20, glideRose.updateItemQuality(19, 1, maxValue = 20))
        assertEquals(20, glideRose.updateItemQuality(20, 1, maxValue = 20))

    }

    @Test
    fun `test item to string format`() {
        val item = Item("+5 Dexterity Vest", 10, 20)
        assertEquals("+5 Dexterity Vest, 10, 20", item.toString())
    }


}


